/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc;

/**
 * Fichero: ModeloEuros.java
 *
 * @date 16-feb-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class ModeloEuros {

  private double cambio;

  public ModeloEuros(double valorCambio) {
// valor en la moneda de 1 euro
    cambio = valorCambio;
  }

  public double eurosAmoneda(double cantidad) {
    return cantidad * cambio;
  }

  public double monedaAeuros(double cantidad) {
    return cantidad / cambio;
  }
}
