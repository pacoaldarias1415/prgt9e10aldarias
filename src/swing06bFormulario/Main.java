/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package swing06bFormulario;

/**
 * Fichero: Main.java
 *
 * @date 03-abr-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Main {

  public static void main(String args[]) {
    ModeloCliente m = new ModeloCliente();
    VistaPantalla v = new VistaPantalla();
    Controlador c = new Controlador(v, m);
  }
}
