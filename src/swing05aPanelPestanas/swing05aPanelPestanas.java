/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package swing05aPanelPestanas;

import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * Fichero: swing05aPanelPestanas.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 31-mar-2014
 */
public class swing05aPanelPestanas extends JFrame {

  public swing05aPanelPestanas() {
    super();
    JPanel principal = new JPanel();
    JLabel label1 = new JLabel("Este es el panel principal");
    principal.add(label1);
    JPanel avanzada = new JPanel();
    JLabel label2 = new JLabel("Este es el panel avanzada");
    avanzada.add(label2);
    JPanel privada = new JPanel();
    JLabel label3 = new JLabel("Este es el panel privada");
    privada.add(label3);
    JPanel email = new JPanel();
    JLabel label4 = new JLabel("Este es el panel email");
    email.add(label4);
    JPanel seguridad = new JPanel();
    JLabel label5 = new JLabel("Este es el panel seguridad");
    seguridad.add(label5);
    JTabbedPane pestanas = new JTabbedPane();
    ImageIcon icon = new ImageIcon("pic.gif");
    pestanas.addTab("Principal", icon, principal);
    pestanas.addTab("Avanzada", icon, avanzada);
    pestanas.addTab("Privada", icon, privada);
    pestanas.addTab("Email", icon, email);
    pestanas.addTab("Seguridad", icon, seguridad);
    /*
     * Se le da tamaño a una pestaña y el resto toma el mismo
     */
    principal.setPreferredSize(new Dimension(410, 50));

    add(pestanas);

    pestanas.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);

    //pack();
    //setVisible(true);
  }

  private void crearYMostrarGUI() {
    //Crear y configurar la ventanaswing06aDosVentanas
    setTitle("Panel con pestañas");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


    //Mostrar la ventana
    pack();
    setVisible(true);
  }

  public static void main(String[] args) {
    swing05aPanelPestanas f = new swing05aPanelPestanas();
    f.crearYMostrarGUI();
  }
}
